#! /usr/bin/python3

from slugify import slugify

def handle(value):
    return slugify(value)

def wordOccurances(searchWord, collection):
    count = 0

    for word in collection:
        if word == searchWord:
            count += 1

    return count

def extractTagsFromProduct(products = []):
    tags = []

    for product in products:
        for tag in product['tags'].split(', '):
            tags.append(tag)

    tags.sort()    
    return set(tags)

# /usr/bin/python3

import json
import secrets
import math
from shopify.shopify import Shopify
from shopify.mapper import ShopifyProductMapper
        
def createStagingSiteDefaults():    
    
    # Reenable
    # createSamplePages()

    # Reenable
    # createSampleProducts()

    # createCollectionsFromProducts("vendor")
    # createCollectionsFromProducts("tags") # TODO: Split array per item in array

    # Create Sample Blog with Articles
    # createSampleBlog()

    # Create sample articles for News Blog
    createSampleArticles(29433561184)

    print(">> Complete")

def createSampleBlog():
    shopifyApi = Shopify(secrets.shopify)

    with open('./samples/blogs.json', "r", encoding="utf-8") as json_file:
        data = json.load(json_file)

    for blog in data:
        res = shopifyApi.articles.createBlog(blog)
        
        blog_id = res['blog']['id']
        createSampleArticles(blog_id)

def createSampleArticles(blog_id):
    shopifyApi = Shopify(secrets.shopify)

    with open('./samples/articles.json', "r", encoding="utf-8") as json_file:
        data = json.load(json_file)

    for article in data:
        res = shopifyApi.articles.createArticle(article, blog_id)
        print(res)

def createSamplePages():
    shopifyApi = Shopify(secrets.shopify)

    with open('./samples/pages.json', "r", encoding="utf-8") as json_file:
        data = json.load(json_file)

    for page in data:
        res = shopifyApi.pages.createPage(page)
        print(res)

def createSampleProducts():
    shopifyApi = Shopify(secrets.shopify)

    with open('./samples/products.json', "r", encoding="utf-8") as json_file:
        data = json.load(json_file)

    for product in data:
        mapper = ShopifyProductMapper(product)

        product = mapper.toShopifyApiProduct()

        res = shopifyApi.products.postProduct(product)
        print(res)

def createCollectionsFromProducts(shopifyFieldName):
    shopifyApi = Shopify(secrets.shopify)

    limitPerPage = 50
    productCount = shopifyApi.products.getProductCount()
    totalPages = math.ceil(productCount / limitPerPage)
    page = 1

    uniqueCollections = set()

    # Get All Vendors from Products in Shopify 
    while page <= totalPages:

        products = shopifyApi.products.getAllProducts({
            "limit": limitPerPage,
            "fields": shopifyFieldName,
            "page": page
        }, page)

        fields = [p[shopifyFieldName] for p in products['products']]
        uniqueCollections.update(fields)

        page += 1

    # Create a collection for each Vendor
    for collection in uniqueCollections:
        payload = {
            "smart_collection": {
                "title": collection,
                "rules": [
                    {
                        "column": 'tag',
                        "relation": "equals",
                        "condition": collection
                    }
                ]
            }
        }

        response = shopifyApi.collections.createCollection(payload, collection)
        print(response)

# /usr/bin/python3

from .client import ShopifyClient

class ShopifyProducts:

    def __init__(self, secrets):
        self.shopify_uri = secrets['shopify_uri']
        self.client = ShopifyClient(secrets)

    def getAllProducts(self, params={}, page=1):
        print(">>> Fetching all products (%s)" % (page))

        shopifyUri = "{0}/{1}".format(
            self.shopify_uri, 'admin/products.json')

        response = self.client.get(shopifyUri, params)

        return response

    def getProduct(self, shopifyProductId):
        print(">>> Fetching single product (%s)" % (shopifyProductId))

        shopifyUri = "{0}/{1}/{2}.json".format(
            self.shopify_uri, 'admin/products', shopifyProductId)

        response = self.client.get(shopifyUri)

        return response

    def postProduct(self, params={}):
        print(">>> Posting product to Shopify(%s)" %
              (params['product']['handle']))

        shopifyUri = "{0}/{1}.json".format(
            self.shopify_uri, 'admin/products')

        response = self.client.post(shopifyUri, params)

        return response

    def uploadImageToProduct(self, shopifyProductId, variantId, filename, base64Attachment, position):
        shopifyUri = "{0}/{1}/{2}/images.json".format(
            self.shopify_uri, 'admin/products', shopifyProductId)

        print(">>> Uploading Image to Product ('%s' at position '%s')" %
              (shopifyProductId, position))

        payload = {
            "image": {
                "variant_ids": [
                    variantId
                ],
                "attachment": base64Attachment,
                "filename": filename,
                "position": position
            }
        }

        response = self.client.post(shopifyUri, payload)

        return response

    def getProductImages(self, shopifyProductId):
        shopifyUri = "{0}/{1}/{2}/images.json".format(
            self.shopify_uri, 'admin/products', shopifyProductId)

        response = self.client.get(shopifyUri)

        return response['images']

    def getProductCount(self):
        print(">>> Fetching product count")

        shopifyUri = "{0}/{1}".format(
            self.shopify_uri, 'admin/products/count.json')

        response = self.client.get(shopifyUri)

        return response['count']

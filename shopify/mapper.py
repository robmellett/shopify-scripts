# /usr/bin/python3
 
import helpers

class ShopifyProductMapper:

    def __init__(self, data):
        self.data = data

    def toShopifyApiProduct(self):       
        return { 
            "product" : {
                "handle": helpers.handle(self.data['name']),
                "title":  self.data['name'],
                "position": 2,
                "body_html": self.data['shortDescription'],
                "vendor": self.data.get('manufacturer'),
                "product_type": self.data['type'],
                "tags": ",".join(filter(None, self.data.get('categories'))),
                "published": True,
                "grams": "",
                "compare_at_price": None,
                "requires_shipping": True,
                "images": [
                    {
                        "src": self.data['image'],
                        "position": 1,
                        "alt": self.data['name']
                    }
                ],
                "variants": [
                    {
                        "option1": "Variant 1",
                        "price": int(self.data['salePrice']),
                        "sku": self.data['objectID'],
                        "inventory_management": "shopify",
                        "inventory_policy": "continue",
                        "inventory_quantity": 10,
                    }
                ]
            }   
        }

    def toShopifyCSVProduct(self):
        #  Do not change the order
        return {
            "Handle": helpers.handle(self.data['name']),
            "Title": self.data['name'],
            "Body(HTML)": self.data['shortDescription'],
            "Vendor": self.data.get('manufacturer'),
            "Type": self.data['type'],
            "Tags": "",
            "Published": "TRUE",
            "Option1 Name": '',
            "Option1 Value": '',
            "Option2 Name": '',
            "Option2 Value": '',	
            "Option3 Name": '',
            "Option3 Value": '',
            "Variant SKU": "SKU" + self.data['objectID'],
            "Variant Grams": "",
            "Variant Inventory Tracker": "shopify",
            "Variant Inventory Policy": "continue",
            "Variant Fulfillment Service": "manual",
            "Variant Price": self.data['salePrice'],
            "Variant Compare At Price": '',
            "Variant Requires Shipping": "TRUE",
            "Variant Barcode": '',
            "Image Src": self.data['image'],
            "Image Position": 1,
            "Image Alt Text": self.data['name'],
            "Gift Card": "FALSE",
            "SEO Title": self.data['name'],
            "SEO Description": self.data['shortDescription'],
            "Google Shopping / Google Product Category": '',
            "Google Shopping / Gender": '',
            "Google Shopping / Age Group": '',
            "Google Shopping / MPN": '',
            "Google Shopping / AdWords Grouping": '',
            "Google Shopping / AdWords Labels": '',
            "Google Shopping / Condition": '',
            "Google Shopping / Custom Product": '',
            "Google Shopping / Custom Label 0": '',
            "Google Shopping / Custom Label 1": '',
            "Google Shopping / Custom Label 2": '',
            "Google Shopping / Custom Label 3": '',
            "Google Shopping / Custom Label 4": '',
            "Variant Image": self.data['image'],
            "Variant Weight Unit": '',
            "Variant Tax Code": '',
            "Cost per item": ''
        }        
    
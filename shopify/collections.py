# /usr/bin/python3

from .client import ShopifyClient

class ShopifyCollections:

    def __init__(self, secrets):
        self.shopify_uri = secrets['shopify_uri']
        self.client = ShopifyClient(secrets)

    def getCollectionCount(self):
        print(">>> Fetching collection count")

        shopifyUri = "{0}/{1}".format(
            self.shopify_uri, 'admin/smart_collections/count.json')

        response = self.client.get(shopifyUri)

        return response['count']

    def getAllCollections(self, limit, page):
        print(">>> Fetching all collections (%s)" % (page))

        shopifyUri = "{0}/{1}".format(
            self.shopify_uri, 'admin/smart_collections.json')

        params = {'limit': limit, 'page': page}

        response = self.client.get(shopifyUri, params)

        return response['smart_collections']

    def getCollectionProducts(self, collection_id):
        print(">>> Fetching all products in collection (%s)" % (collection_id))

        shopifyUri = "{0}/{1}".format(
            self.shopify_uri, 'admin/products.json')

        params = {'collection_id': collection_id}

        response = self.client.get(shopifyUri, params)

        return response['products']

    def createCollection(self, params={}, title=""):
        print(">>> Creating collection %s" % (title))

        shopifyUri = "{0}/{1}".format(
            self.shopify_uri, 'admin/smart_collections.json')

        response = self.client.post(shopifyUri, params)

        return response

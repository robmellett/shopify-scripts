# /usr/bin/python3

from .articles import ShopifyArticles
from .collections import ShopifyCollections
from .products import ShopifyProducts
from .pages import ShopifyPages

class Shopify:

    def __init__(self, secrets):
        self.secrets = secrets

        self.articles = ShopifyArticles(secrets)
        self.collections = ShopifyCollections(secrets)
        self.products = ShopifyProducts(secrets)
        self.pages = ShopifyPages(secrets)

    def articles(self):
        return self.articles

    def collections(self):
        return self.collections

    def products(self):
        return self.products

    def pages(self):
        return self.pages

# /usr/bin/python3

from .client import ShopifyClient

class ShopifyArticles:

    def __init__(self, secrets):
        self.shopify_uri = secrets['shopify_uri']
        self.client = ShopifyClient(secrets)

    def createBlog(self, params={}, title=""):
        print(">>> Creating blog %s" % (title))

        shopifyUri = "{0}/admin/api/2019-04/blogs.json".format(
            self.shopify_uri)

        response = self.client.post(shopifyUri, params)

        return response

    def createArticle(self, params={}, blog_id="", title=""):
        print(">>> Creating article %s" % (title))

        shopifyUri = "{0}/admin/api/2019-04/blogs/{1}/articles.json".format(
            self.shopify_uri,  blog_id)

        response = self.client.post(shopifyUri, params)

        return response

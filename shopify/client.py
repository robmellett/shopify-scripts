# /usr/bin/python

import requests
import secrets
from requests.auth import HTTPBasicAuth

class ShopifyClient:
    def __init__(self, secrets):
        self.secrets = secrets

    def get(self, uri, params={}):
        json = requests.get(
            uri,
            headers={
                "Content-Type": "application/json",
            },
            auth=HTTPBasicAuth(self.secrets['shopify_user'],
                               self.secrets['shopify_password']),
            params=params).json()

        return json

    def post(self, uri, params={}):
        
        request = requests.post(
            uri,
            headers={
                "Content-Type": "application/json",
            },
            auth=HTTPBasicAuth(self.secrets['shopify_user'],
                               self.secrets['shopify_password']),
            json=params
        ).json()

        return request

    def delete(self, uri, params={}):
        request = requests.delete(
            uri,
            headers={
                "Content-Type": "application/json",
            },
            auth=HTTPBasicAuth(self.secrets['shopify_user'],
                               self.secrets['shopify_password'])
        ).json()

        return request

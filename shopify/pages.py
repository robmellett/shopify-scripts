# /usr/bin/python3

from .client import ShopifyClient

class ShopifyPages:

    def __init__(self, secrets):
        self.shopify_uri = secrets['shopify_uri']
        self.client = ShopifyClient(secrets)

    def createPage(self, payload):
        print(">>> Creating Page")

        shopifyUri = "{0}/{1}".format(
            self.shopify_uri, 'admin/api/2019-04/pages.json')

        response = self.client.post(shopifyUri, payload)

        return response

    def getAllPages(self):
        print(">>> Creating Page")

        shopifyUri = "{0}/{1}".format(
            self.shopify_uri, 'admin/api/2019-04/pages.json')

        response = self.client.get(shopifyUri)

        return response
        

    

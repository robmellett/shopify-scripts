https://www.json-generator.com

words: '{{lorem(1, "words")}}'

paragraphs: '{{lorem(1, "paragraphs")}}',

## Blog
```json
[
  '{{repeat(5, 7)}}',
  {
    "blog": {
      title: '{{lorem(5, "words")}}'
    }
  }
]
```

## Article
```json
[
  '{{repeat(5, 7)}}',
  {
    "article": {
      title: '{{lorem(5, "words")}}',
      body_html: '{{lorem(2, "paragraphs")}}',
      summary_html: '{{lorem(1, "paragraphs")}}',
      tags: [
        '{{repeat(7)}}',
        '{{lorem(1, "words")}}'
      ],
      "image" : {
        "src": "https://source.unsplash.com/1600x900/?busines,work?",
        "alt": '{{lorem(5, "words")}}'
      }
    } 
  }
]
```